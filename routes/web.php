<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->name('admin.')->group(function () {

    Route::get('/', 'RequisitionController@index');
    Route::get('/home', 'RequisitionController@index')->name('home');
    Route::post('/requests/{request}/approve', 'RequisitionController@approve')->name('requests.approve');
    Route::get('/requests/{request}/cancel', 'RequisitionController@cancellation')->name('requests.cancellation');
    Route::post('/requests/{request}/cancel', 'RequisitionController@cancel')->name('requests.cancel');
    Route::post('/requests/{request}/add-message', 'RequisitionController@addMessage')->name('requests.messages.add');
    Route::post('/requests/{request}/mark-message-read', 'RequisitionController@markMessageRead')->name('requests.messages.read');
    Route::resource('/requests', 'RequisitionController');
    /*Route::post('/pulls/{pull}/add-message', 'PullsController@addMessage')->name('pulls.messages.add');*/
    Route::put('/pulls/{pull}/pulled', 'PullsController@pulled')->name('pulls.pulled');
    Route::put('/pulls/{pull}/missing', 'PullsController@missing')->name('pulls.missing');
    Route::get('/pulls/{pull}/print', 'PullsController@print')->name('pulls.print');
    Route::resource('/pulls', 'PullsController');

});

Route::middleware(['auth'])->prefix('admin')->name('admin.')->namespace('Admin')->group(function () {


    Route::get('/users/subjects/edit', 'SubjectUserController@edit')->name('users.subjects.edit');
    Route::put('/users/subjects/update', 'SubjectUserController@update')->name('users.subjects.update');

    Route::resource('/users', 'UserController');

    Route::get('/dashboard', 'DashboardController')->name('dashboard');

});

Auth::routes(['register' => false]);
