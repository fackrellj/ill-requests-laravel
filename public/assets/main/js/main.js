/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./source/_assets/js/main.js":
/***/ (function(module, exports) {

window.mckay = {

    init: function init() {

        mckay.showHours();
    },

    showHours: function showHours() {}
};

$(function () {

    // Add hours to homepage
    var $hoursContainer = $('#hours-button');
    $.ajax({
        url: "https://api3.libcal.com/api_hours_today.php?iid=4251&lid=0&format=json&systemTime=0",
        type: "GET",
        dataType: 'jsonp',
        timeout: 3000,
        success: function success(data) {

            var location = data.locations[0];
            if (location.times.currently_open == true) {
                $hoursContainer.html('Today\'s Hours: ' + location.rendered);
            } else {
                $hoursContainer.html('Closed');
            }

            if (data.hasOwnProperty('NOTICE')) {
                var notice = '<br /><span style="font-size: 11px;">' + data.NOTICE + '</span>';
                $hoursContainer.append(notice);
            }

            $hoursContainer.parents('div#hours-container').show();
        },
        error: function error(jqXHR, textStatus, errorThrown) {
            $hoursContainer.parents('div#hours-container').hide();
        }
    });
    // End add hours to homepage

    // Open tutorial in new window
    $(document).on('click', 'a.tutorial-link', function (event) {
        event.preventDefault();
        var windowLeft = parseInt(screen.availWidth / 2 - 840 / 2);
        var windowTop = parseInt(screen.availHeight / 2 - 720 / 2);
        var windowSize = "width=840,height=720,left=" + windowLeft + ",top=" + windowTop + ",scrollbars=yes";
        var url = $(this).attr('href');
        var windowName = "tutorial";
        window.open(url, windowName, windowSize);
    });
    // End open tutorial in new window

});

/***/ }),

/***/ "./source/_assets/sass/main.scss":
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./source/_assets/js/main.js");
module.exports = __webpack_require__("./source/_assets/sass/main.scss");


/***/ })

/******/ });