<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The Format enum.
 *
 * @method static self HARDBACK()
 * @method static self SOFTBACK()
 * @method static self EBOOK()
 * @method static self ABOOK()
 * @method static self MEDIA()
 * @method static self OTHER()
 */
class Format extends Enum
{
    const HARDBACK = 'HARDBACK';
    const SOFTBACK = 'SOFTBACK';
    const EBOOK = 'EBOOK';
    const ABOOK = 'ABOOK';
    const MEDIA = 'MEDIA';
    const OTHER = 'OTHER';

    /**
     * Retrieve a map of enum keys and values.
     *
     * @return array
     */
    public static function map() : array
    {
        return [
            static::HARDBACK => 'Hardback Book',
            static::SOFTBACK => 'Softback Book',
            static::EBOOK => 'Electronic Book',
            static::ABOOK => 'Audio Book',
            static::MEDIA => 'Media',
            static::OTHER => 'Other',
        ];
    }
}
