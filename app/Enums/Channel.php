<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The Format enum.
 *
 * @method static self ILL()
 * @method static self PURCHASE()
 */
class Channel extends Enum
{
    const ILL = 'ILL';
    const PURCHASE = 'PURCHASE';

    /**
     * Retrieve a map of enum keys and values.
     *
     * @return array
     */
    public static function map() : array
    {
        return [
            static::ILL => 'ILL Request',
            static::PURCHASE => 'Purchase Request',
        ];
    }
}
