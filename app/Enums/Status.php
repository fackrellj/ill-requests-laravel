<?php

namespace App\Enums;

use Rexlabs\Enum\Enum;

/**
 * The Status enum.
 *
 * @method static self NEW_PURCHASE_REQUEST()
 * @method static self PENDING_APPROVAL()
 * @method static self APPROVED_FOR_ILL()
 * @method static self APPROVED_FOR_PURCHASE()
 * @method static self ORDERED()
 * @method static self RECEIVED()
 * @method static self DELIVERED()
 * @method static self CANCELLED()
 */
class Status extends Enum
{
    const NEW_PURCHASE_REQUEST = 9;
    const PENDING_APPROVAL = 1;
    const APPROVED_FOR_ILL = 2;
    const APPROVED_FOR_PURCHASE = 3;
    const ORDERED = 4;
    const RECEIVED = 5;
    const DELIVERED = 6;
    const CANCELLED = 7;
    const NOT_AVAILABLE_FOR_PURCHASE = 8;

    /**
     * Retrieve a map of enum keys and values.
     *
     * @return array
     */
    public static function map() : array
    {
        return [
            static::NEW_PURCHASE_REQUEST => 'New Purchase Request',
            static::PENDING_APPROVAL => 'Pending Approval',
            static::APPROVED_FOR_ILL => 'Approved for ILL',
            static::APPROVED_FOR_PURCHASE => 'Approved for Purchase',
            static::ORDERED => 'Ordered',
            static::RECEIVED => 'Received',
            static::DELIVERED => 'Delivered',
            static::CANCELLED => 'Cancelled',
            static::NOT_AVAILABLE_FOR_PURCHASE => 'Not Available for Purchase',
        ];
    }
}
