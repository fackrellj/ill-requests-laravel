<?php


if( ! function_exists('user') ) {
    function user() {
        return auth()->user();
    }
}

if( ! function_exists('patron') ) {
    function patron() {
        return auth('patrons')->user();
    }
}
