<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Wildside\Userstamps\Userstamps;

class Cancellation extends Model
{
    use Userstamps;

    protected $fillable = [];
}
