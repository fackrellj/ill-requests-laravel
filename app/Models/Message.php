<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'requisition_id', 'messageable_id', 'messageable_type', 'body',
    ];

    /**
     * Get all of the owning imageable models.
     */
    public function messageable()
    {
        return $this->morphTo();
    }
}
