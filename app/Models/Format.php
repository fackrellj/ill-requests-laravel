<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Format extends Model
{
    protected $fillable = [
        'type',
        'isbn',
        'edition',
        'cost',
        'link',
        'notes'
    ];
}
