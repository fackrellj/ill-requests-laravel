<?php

namespace App;

use App\Enums\Status;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Requisition extends Model
{
    use SoftDeletes, Userstamps;

    protected $fillable = [
        'channel',
    ];

    protected $dates = [
        'approved_at',
        'purchase_request_sent_at',
        'received_at',
    ];

    public function patron()
    {
        return $this->belongsTo(Patron::class);
    }

    public function librarian()
    {
        return $this->belongsTo(User::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function formats()
    {
        return $this->hasMany(Format::class);
    }

    /**
     * The messages for the job
     */
    public function messages()
    {
        return $this->hasMany(Message::class)
                    ->orderBy('created_at', 'DESC');
    }

    public function getDisplayTnNumberAttribute() {
        if(is_null($this->tn_number)){
            return 'Purchase Request';
        }
        return $this->tn_number;
    }

    /**
     * Scope a query to requests needing attention.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNewPurchase($query)
    {
        return $query->where('status', Status::NEW_PURCHASE_REQUEST);
    }

    /**
     * Scope a query to requests needing attention.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNewPull($query)
    {
        return $query->where('pulled', 0);
    }

    /**
     * Scope a query to requests needing attention.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAttention($query)
    {
        return $query->where('ill_needs_updated', 1);
    }

    /**
     * Scope a query to overdue requests.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOverdue($query)
    {
        return $query->where('created_at', '<', now()->subHours(24))
                        ->where('status', Status::PENDING_APPROVAL);
    }

    /**
     * Scope a query to ill requests.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIll($query)
    {
        return $query->where('status', Status::APPROVED_FOR_ILL);
    }

    /**
     * Scope a query to ordered requests.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrdered($query)
    {
        return $query->where('status', Status::ORDERED);
    }

    /**
     * Scope a query to cancelled requests.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCancelled($query)
    {
        return $query->where('status', Status::CANCELLED);
    }

    /**
     * Scope a query to limit requests to user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLimitToUser($query)
    {
        return $query->where('librarian_id', user()->id);
    }

    /**
     * Scope a query to limit requests to search terms.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $term)
    {
        return $query->where(function(Builder $query) use ($term){
            $query->whereHas('patron', function(Builder $q) use ($term){
                $q->where('name', 'LIKE', '%'.$term.'%');
            })
                ->orWhere('tn_number', 'LIKE', '%'.$term.'%')
                ->orWhere('title', 'LIKE', '%'.$term.'%');
        });
    }

    /**
     * Scope a query to limit requests to status.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * Scope a query to limit requests to status.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasMessage($query)
    {
        return $query->where('librarian_added_note', 1);
    }

}
