<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Patron extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'inumber',
    ];

    public function getRolesAttribute($value) {
        return explode('|', $value);
    }

    public function setRolesAttribute($value) {
        if(is_array($value)){
            $this->attributes['roles'] = implode('|', $value);
        }else{
            $this->attributes['roles'] = '';
        }

    }

    /**
     * Get the patron's avatar link.
     *
     */
    public function getAvatarAttribute() {
        if(in_array('Employee', $this->roles)){
            return 'https://web.byui.edu/Directory/Employee/' . explode('@', $this->email)[0] . '.jpg';
        }
        return 'https://web.byui.edu/Directory/Student/' . explode('@', $this->email)[0] . '.jpg';
    }

    public function getFirstNameAttribute() {
        return explode(' ', $this->name)[0];
    }

    public function getLastFirstNameAttribute() {
        $name = explode(' ', $this->name);

        return end($name) . ', ' . $name[0];
    }

    public function getAffiliationAttribute() {
        return ((in_array('Employee', $this->roles))?'Employee':'Student');
    }

    static function findOrCreatePatronFromRequest($username){
        $patron = Patron::where('username', $username)->first();

        if(!is_null($patron)){
            return $patron;
        }

        $institution_username = "byui:$username";

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => env('BYUI_OAUTH_CLIENTID'),    // The client ID assigned to you by the provider
            'clientSecret' => env('BYUI_OAUTH_CLIENTSECRET'),   // The client password assigned to you by the provider
            'redirectUri' => 'https://www.getpostman.com/oauth2/callback',
            'urlAuthorize' => env('BYUI_API_BASE_URL') . '/authorize',
            'urlAccessToken' => env('BYUI_API_BASE_URL') . '/token',
            'urlResourceOwnerDetails' => 'https://ids.byui.edu/oauth2/userinfo?schema=openid'
        ]);

        $accessToken = $provider->getAccessToken('client_credentials');
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', env('BYUI_API_BASE_URL') . "/librarybridge/v1/cclaUser/".trim($institution_username),
            [
                'connect_timeout' => 20,
                'allow_redirects' => true,
                'timeout' => 2000,
                'headers' => [
                    'Authorization' => "Bearer $accessToken"
                ]
            ]);

        // Here the code for successful request

        $byuiUser = json_decode($response->getBody());

        $patron = new Patron();
        $patron->username = $username;
        $patron->name = $byuiUser->prefferedName . ' ' . $byuiUser->surname;
        if(strlen($byuiUser->personalContact->email) > 5){
            $patron->email = $byuiUser->personalContact->email;
        }else{
            $patron->email = $byuiUser->workContact->email;
        }
        $patron->inumber = $byuiUser->userId;
        $patron->roles = $byuiUser->roles;
        $patron->save();

        return $patron;

    }

    /**
     * Get all of the patron's requests.
     */
    public function requisitions()
    {
        return $this->hasMany(Requisition::class)->orderBy('created_at', 'DESC');
    }

    /**
     * Get all of the patron's messages.
     */
    public function messages()
    {
        return $this->morphMany(Message::class, 'messageable');
    }

}
