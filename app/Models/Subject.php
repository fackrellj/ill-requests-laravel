<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'purchase_subjects';

    public function librarian()
    {
        return $this->hasOne(SubjectUser::class, 'subject_id');
    }
}
