<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wildside\Userstamps\Userstamps;

class Pull extends Model
{

    use SoftDeletes, Userstamps;

    public function patron()
    {
        return $this->belongsTo(Patron::class);
    }

    /**
     * The messages for the job
     */
    /*public function messages()
    {
        return $this->hasMany(Message::class)->orderBy('created_at', 'DESC');
    }*/

    /**
     * Scope a query to new pull requests.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNewpull($query)
    {
        return $query->where('pulled', 0);
    }

}
