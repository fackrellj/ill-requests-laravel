<?php

namespace App\Http\Controllers;

use App\Message;
use App\Notifications\PullRequestReady;
use App\Pull;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class PullsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pulls = Pull::where('pulled', 0);

        if($request->has('status')){
            $pulls = new Pull();
            if($request->get('status') == 'pending'){
                $pulls = $pulls->where('pulled', 0);
            }

            if($request->get('status') == 'pulled'){
                $pulls = $pulls->where('pulled', 1);
            }

            if($request->get('status') == 'cancelled'){
                $pulls = $pulls->onlyTrashed();
            }

            if($request->get('status') == 'all'){
                $pulls = $pulls->withTrashed();
            }
        }

        if($request->has('q')){
            $pulls = $pulls->where(function(Builder $query) use ($request){
                $query->whereHas('patron', function(Builder $q) use ($request){
                    $q->where('name', 'LIKE', '%'.$request->get('q').'%');
                })
                    ->orWhere('title', 'LIKE', '%'.$request->get('q').'%');
            });
        }

        $pulls = $pulls->paginate(25);

        return view('admin.ill.pulls.index', [
            'pulls' => $pulls,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Pull $pull
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pull = Pull::where('id', $id)->withTrashed()->first();

        return view('admin.ill.pulls.show' , [
            'pull' => $pull
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Pull $pull
     * @return \Illuminate\Http\Response
     */
    public function edit(Pull $pull)
    {
        $patron = $pull->patron;

        return view('admin.ill.pulls.edit', [
            'pull' => $pull,
            'patron' => $patron,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pull $pull)
    {
        $pull->title = $request->get('title');
        $pull->author = $request->get('author');
        $pull->call_number = $request->get('call_number');
        $pull->location = $request->get('location');
        $pull->barcode = $request->get('barcode');
        $pull->save();

        return redirect()->route('admin.pulls.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Pull $pull
     * @return \Illuminate\Http\Response
     */
    public function pulled(Request $request, $id)
    {
        $pull = Pull::withTrashed()->where('id', $id)->first();

        if($pull->patron->affiliation == 'Student') {
            $client = new \GuzzleHttp\Client();

            $response = $client->request('GET', 'http://abish.byui.edu/horizon/api/index.cfm/pulled/' . $request->get('barcode'),
                [
                    'connect_timeout' => 20,
                    'allow_redirects' => true,
                    'timeout' => 2000,
                    'headers' => [
                        'Authorization' => "Bearer " . env('HORIZON_API_KEY')
                    ]
                ]);
            $pull->barcode = $request->get('barcode');
        }

        $pull->pulled = 1;
        $pull->save();

        if($pull->patron->affiliation == 'Student'){
            $pull->patron->notify(new PullRequestReady($pull));
        }

        return redirect()->route('admin.pulls.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Pull $pull
     * @return \Illuminate\Http\Response
     */
    public function missing(Request $request, Pull $pull)
    {

        switch($request->get('action')){


            case 'not-found':
                $pull->patron->notify(new \App\Notifications\PullNotFound($pull));
                /*$pull->messages()->save(new Message([
                    'body' => user()->name . ' could not find this item.',
                ]));*/
                break;

            case 'lost':
                $pull->patron->notify(new \App\Notifications\PullCancelled($pull));
                /*$pull->messages()->save(new Message([
                    'body' => user()->name . ' cancelled this pull request.',
                ]));*/
                $pull->delete();
                break;
        }

        return redirect()->route('admin.pulls.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Pull $pull
     * @return \Illuminate\Http\Response
     */
    public function print(Request $request, Pull $pull)
    {

        return view('admin.ill.pulls.print' , [
            'pull' => $pull
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pull $pull)
    {
        $pull->delete();

        return redirect()->route('admin.pulls.index');
    }
}
