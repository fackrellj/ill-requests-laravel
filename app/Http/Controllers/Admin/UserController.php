<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Permission;
use App\Role;

class UserController extends \App\Http\Controllers\Controller
{
    use SendsPasswordResetEmails;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereHas("roles", function($q){ $q->where("name", "LIKE", "ILL%"); })->orderBy('name')->get();


        return view('admin.users.index', [
            'users' => $users,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->name = $request->get('name');
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->receive_purchase_notifications = $request->get('receive_purchase_notifications', 0);
        $user->password = Str::random(10);
        $user->save();

        $user->syncRoles('ILL Employee');

        $this->sendResetLinkEmail($request);

        return redirect()->route('admin.users.edit', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        return view('admin.users.edit', [
            'user' => $user,
            'roles' => Role::where('name', 'LIKE', 'ILL%')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->name = $request->get('name');
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->receive_purchase_notifications = $request->get('receive_purchase_notifications', 0);
        $user->save();

        $user->syncRoles($request->get('role'));

        return redirect()->route('admin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('admin.users.index');
    }
}
