<?php

namespace App\Http\Controllers\Admin;

use App\Subject;
use App\SubjectUser;
use App\User;
use Illuminate\Http\Request;

class SubjectUserController extends \App\Http\Controllers\Controller
{

    /**
     * Show the form for editing the specified resource.
     *

     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        return view('admin.users.subjects.edit', [
            'users' => User::whereHas("roles", function($q){ $q->where("name", 'NOT LIKE', "ILL%"); })->orderBy('name', 'ASC')->get(),
            'subjects' => Subject::with('librarian')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $formatData = collect($request->only('subjects', 'librarians'));

        if($formatData->count() > 0) {
            $subjectUsers = $formatData->transpose()->map(function ($data) {

                if($data[1] > 0){
                    $subjectUser = SubjectUser::updateOrCreate([
                        'subject_id' => $data[0],
                    ],[
                        'user_id' => $data[1],
                    ]);
                    return $subjectUser;
                }


            });
        }

        return redirect()->back();
    }
}
