<?php

namespace App\Http\Controllers\Admin;

use App\Charts\PullRequestsByUserType;
use App\Pull;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends \App\Http\Controllers\Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {


        $pullRequestsByUserType = new PullRequestsByUserType;
        $pullRequestsByUserType->dataset('Students', 'line', collect([])->values());

        return view('admin.dashboard.index', [
            'pullRequestsByUserType' => $pullRequestsByUserType,
        ]);
    }
}
