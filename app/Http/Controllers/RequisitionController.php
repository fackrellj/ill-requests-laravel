<?php

namespace App\Http\Controllers;

use App\Enums\Channel;
use App\Enums\Status;
use App\Format;
use App\Mail\NewMessage;
use App\Message;
use App\Notifications\NewPurchaseOrder;
use App\Notifications\PendingILLRequest;
use App\Notifications\PendingPurchaseRequest;
use App\Notifications\PurchaseRequestApproved;
use App\Notifications\PurchaseRequestReceived;
use App\Notifications\RequestApprovalReset;
use App\Patron;
use App\Pull;
use App\Requisition;
use App\Subject;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Validation\Rule;
use Psr\Log\NullLogger;

class RequisitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $requisitions = new Requisition();

        if(!user()->hasRole('ILL Employee') && !user()->hasRole('ILL Purchasing') && user()->email != 'ill@byui.edu'){
            $requisitions = $requisitions->limitToUser();
        }

        if($request->has('status') && in_array($request->get('status'), Status::keys())){
            if($request->get('status') == Status::PENDING_APPROVAL){
                if($request->has('state') && $request->get('state') == 'overdue'){
                    $requisitions = $requisitions->overdue();
                }
            }else{
                $requisitions = $requisitions->status($request->get('status'));
            }
        }else if(user()->hasRole('ILL Purchasing')){
            $requisitions = $requisitions->status(Status::APPROVED_FOR_PURCHASE);
        }else{
            $requisitions = $requisitions->whereNotIn('status', [Status::DELIVERED]);
        }

        if($request->has('q')){
            $requisitions = $requisitions->search($request->get('q'));
        }

        if($request->has('messages')){
            $requisitions = $requisitions->hasMessage();
        }

        $requisitions = $requisitions->orderBy('created_at', 'DESC')->paginate(25);

        $stats = [];
        $stats['new_purchase_request'] = Requisition::newPurchase()->count();
        $stats['new_pull_request'] = Pull::newPull()->count();
        $stats['overdue'] = Requisition::overdue()->count();
        $stats['ill_approved'] = Requisition::ill()->attention()->count();
        $stats['ordered'] = Requisition::ordered()->attention()->count();
        $stats['cancelled'] = Requisition::cancelled()->attention()->count();
        $stats['messages'] = Requisition::hasMessage()->count();

        $statuses = [null => '-- Select option --'] + array_combine(Status::keys(), Status::values());

        return view('admin.ill.requisitions.index', [
            'requisitions' => $requisitions,
            'stats' => $stats,
            'statuses' => $statuses,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $requisition =  new \App\Requisition(['channel' => Channel::ILL]);
        $patron =  new \App\Patron();

        $subjects = Subject::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $subjects = [null => '-- Select one --'] + $subjects;
        $librarians = User::whereHas("roles", function($q){ $q->where("name", 'NOT LIKE', "ILL%"); })->orderBy('name', 'ASC')->get()->pluck('name', 'id')->all();
        $librarians = [null => '-- Select one --'] + $librarians;
        $librarianSubjects = DB::table('subject_user')->pluck('user_id', 'subject_id')->all();

        if($request->has('tn_number')){
            $client = new \GuzzleHttp\Client();

            $response = $client->request('GET', env('ILLIAD_API_BASE_URL'). '/Transaction/' . $request->get('tn_number'),
                [
                    'connect_timeout' => 20,
                    'allow_redirects' => true,
                    'timeout' => 2000,
                    'headers' => [
                        'ApiKey' => env('ILLIAD_API_KEY'),
                        'Accept' => 'application/json; version=1',
                    ]
                ]);

            $pendingRequest = json_decode($response->getBody());

            //dd($pendingRequest);

            $requisition->tn_number = $pendingRequest->TransactionNumber;
            $requisition->title = $pendingRequest->LoanTitle;
            $requisition->author = $pendingRequest->LoanAuthor;

            Try{
                $username = "byui:" . $pendingRequest->Username;

                $provider = new \League\OAuth2\Client\Provider\GenericProvider([
                    'clientId' => env('BYUI_OAUTH_CLIENTID'),    // The client ID assigned to you by the provider
                    'clientSecret' => env('BYUI_OAUTH_CLIENTSECRET'),   // The client password assigned to you by the provider
                    'redirectUri' => 'https://www.getpostman.com/oauth2/callback',
                    'urlAuthorize' => env('BYUI_API_BASE_URL') . '/authorize',
                    'urlAccessToken' => env('BYUI_API_BASE_URL') . '/token',
                    'urlResourceOwnerDetails' => 'https://ids.byui.edu/oauth2/userinfo?schema=openid'
                ]);

                $accessToken = $provider->getAccessToken('client_credentials');
                $client = new \GuzzleHttp\Client();

                $response = $client->request('GET', env('BYUI_API_BASE_URL') . "/librarybridge/v1/cclaUser/".trim($username),
                    [
                        'connect_timeout' => 20,
                        'allow_redirects' => true,
                        'timeout' => 2000,
                        'headers' => [
                            'Authorization' => "Bearer $accessToken"
                        ]
                    ]);

                // Here the code for successful request

                $byuiUser = json_decode($response->getBody());
            }catch(\Exception $exception){
                return redirect()->back();
            }


            $patron->username = $pendingRequest->Username;
            $patron->name = $byuiUser->prefferedName . ' ' . $byuiUser->surname;
            $patron->inumber = $byuiUser->userId;
            $patron->roles = $byuiUser->roles;
            if(strlen($byuiUser->personalContact->email) > 5){
                $patron->email = $byuiUser->personalContact->email;
            }else{
                $patron->email = optional($byuiUser->workContact)->email;
            }
        }

        return view('admin.ill.requisitions.create', [
            'requisition' => $requisition,
            'patron' => $patron,
            'subjects' => $subjects,
            'librarians' => $librarians,
            'librarianSubjects' => $librarianSubjects,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tn_number' => 'unique:requisitions',
            'email' => 'required|email:rfc,dns',
        ]);

        // TODO: I would really like to be able to call the api and get more info
        $patron = Patron::firstOrCreate([
                'email' => $request->get('email')
            ],
            [
                'name' => $request->get('name'),
                'username' => $request->get('username'),
                'inumber' => $request->get('inumber'),
                'roles' => $request->get('roles'),
            ]);

        $requisition = new Requisition();
        $requisition->tn_number = $request->get('tn_number');
        $requisition->title = $request->get('title');
        $requisition->author = $request->get('author');
        $requisition->subject_id = $request->get('subject_id');
        $requisition->librarian_id = $request->get('librarian_id');
        $requisition->status = Status::PENDING_APPROVAL;
        $requisition->channel = Channel::ILL;
        $requisition->ill_available = $request->get('ill_available', 1);
        $requisition->ill_note = $request->get('ill_note');

        $patron->requisitions()->save($requisition);

        $formatData = collect($request->only('type', 'isbn', 'edition', 'cost', 'link', 'notes'));

        if($formatData->count() > 0) {
            $formats = $formatData->transpose()->map(function ($data) {
                return new Format([
                    'type' => $data[0],
                    'isbn' => $data[1],
                    'edition' => $data[2],
                    'cost' => $data[3],
                    'link' => $data[4],
                    'notes' => $data[5],
                ]);
            });

            $requisition->formats()->saveMany($formats);
        }
        if(!is_null($requisition->librarian_id)){
            $requisition->librarian->notify(new PendingILLRequest($requisition));

            $requisition->messages()->save(new Message([
                'body' => 'Request created and sent to ' . $requisition->librarian->name . ' for approval',
            ]));
        }

        return redirect()->route('admin.requests.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Requisition $requisition)
    {

        return view('admin.ill.requisitions.show', [
            'requisition' => $requisition,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Requisition $requisition)
    {
        $patron = $requisition->patron;
        $subjects = Subject::orderBy('name', 'ASC')->pluck('name', 'id')->all();
        $subjects = [null => '-- Select one --'] + $subjects;
        $librarians = User::whereHas("roles", function($q){ $q->where("name", 'NOT LIKE',"ILL%"); })->orderBy('name', 'ASC')->get()->pluck('name', 'id')->all();
        $librarians = [null => '-- Select one --'] + $librarians;
        $librarianSubjects = DB::table('subject_user')->pluck('user_id', 'subject_id')->all();

        return view('admin.ill.requisitions.edit', [
            'requisition' => $requisition,
            'patron' => $patron,
            'subjects' => $subjects,
            'librarians' => $librarians,
            'librarianSubjects' => $librarianSubjects,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Requisition $requisition)
    {

        /*$request->validate([
            'tn_number' => Rule::unique('requisitions')->ignore($requisition),
        ]);*/

        $librarianId = $requisition->librarian_id;

        $requisition->tn_number = $request->get('tn_number');
        $requisition->title = $request->get('title');
        $requisition->author = $request->get('author');
        $requisition->subject_id = $request->get('subject_id');
        $requisition->librarian_id = $request->get('librarian_id');
        $requisition->ill_available = $request->get('ill_available', 1);
        $requisition->ill_note = $request->get('ill_note');
        $requisition->save();

        // I don't want to over-complicate this next part
        // When editing and saving, I'm just going to recreate the formats
        // and then delete the old ones.

        $formatIds = $requisition->formats->pluck('id')->all();

        $formatData = collect($request->only('type', 'isbn', 'edition', 'cost', 'link', 'notes'));

        if($formatData->count() > 0) {
            $formats = $formatData->transpose()->map(function ($data) {
                return new Format([
                    'type' => $data[0],
                    'isbn' => $data[1],
                    'edition' => $data[2],
                    'cost' => $data[3],
                    'link' => $data[4],
                    'notes' => $data[5],
                ]);
            });

            $requisition->formats()->saveMany($formats);
        }

        Format::destroy($formatIds);

        if($librarianId != $requisition->librarian_id){
            if($requisition->channel == 'ILL'){
                $requisition->librarian->notify(new PendingILLRequest($requisition));
            }else{
                $requisition->librarian->notify(new PendingPurchaseRequest($requisition));
            }
            $requisition->status = Status::PENDING_APPROVAL;
            $requisition->save();
            $requisition->messages()->save(new Message([
                'body' => 'Email sent to ' . $requisition->librarian->name . ' by ' . user()->name,
            ]));
        }

        return redirect()->route('admin.requests.show', [
            'request' => $requisition,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Requisition $requisition
     * @return \Illuminate\Http\Response
     */
    public function addMessage(Request $request, Requisition $requisition)
    {
        $message = new Message([
            'requisition_id' => $requisition->id,
            'body' => $request->get('body'),
        ]);
        user()->messages()->save($message);

        if(!user()->hasRole('ILL Employee')){
            $requisition->librarian_added_note = 1;
            $requisition->save();

            Notification::route('mail', 'ill@byui.edu')
                ->notify(new \App\Notifications\NewMessage($requisition, $message));
        }

        return redirect()->back();
    }

    /**
     * Mark message read.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Requisition $requisition
     * @return \Illuminate\Http\Response
     */
    public function markMessageRead(Request $request, Requisition $requisition)
    {
        $requisition->librarian_added_note = 0;
        $requisition->save();

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Requisition $requisition
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, Requisition $requisition)
    {
        $logMessage = null;

        switch($request->get('action')){

            case 'BUY':
                $status = Status::APPROVED_FOR_PURCHASE;
                $logMessage = 'Request approved for Purchase by ' . user()->name;

                $requisition->approved_for = Channel::ILL;
                $requisition->approved_at = now();
                $requisition->purchase_request_sent_at = now();

                Format::whereIn('id', $request->get('format'))->update(['approval' => Status::APPROVED_FOR_PURCHASE]);

                $formats = Format::whereIn('id', $request->get('format'))->get();

                Notification::send(User::where('receive_purchase_notifications', 1)->get() , new NewPurchaseOrder($requisition, $formats));

                break;

            case 'BORROW':
                $status = Status::APPROVED_FOR_ILL;
                $logMessage = 'Request approved for ILL by ' . user()->name;

                $requisition->approved_for = Channel::PURCHASE;
                $requisition->approved_at = now();
                $requisition->ill_needs_updated = 1;

                // Dashboard will be updated. Nothing else needs to be done.
                break;

            case 'CANCEL':
                $status = Status::CANCELLED;
                $logMessage = 'Request cancelled by ' . user()->name;

                $requisition->approved_for = 'CANCELLED';
                $requisition->approved_at = now();
                $requisition->ill_needs_updated = 1;

                // Item will be cancelled. Nothing else needs to be done.
                break;

            case 'RESET':
                $resetFormats = true;
                if($requisition->status == Status::APPROVED_FOR_ILL){
                    $resetFormats = false;
                }
                $status = Status::PENDING_APPROVAL;
                $logMessage = 'Request set to Pending Approval by ' . user()->name;
                $requisition->approved_for = null;
                $requisition->approved_at = null;
                $requisition->purchase_request_sent_at = null;

                if($resetFormats){
                    Format::whereIn('id', $request->get('format'))->update(['approval' => null]);
                    Notification::send(User::where('receive_purchase_notifications', 1)->get() , new RequestApprovalReset($requisition));
                }

                break;

            case 'RECEIVED':
                $status = Status::RECEIVED;
                if($requisition->channel == Channel::ILL){
                    $requisition->ill_needs_updated = 0;
                }
                $logMessage = 'Request Received in ILL by ' . user()->name;

                // Faculty don't need to be notified
                if(!in_array('Employee', $requisition->patron->roles)){
                    $requisition->patron->notify(new PurchaseRequestReceived($requisition));
                }

                break;

            case 'ILL_UPDATED':
                $requisition->ill_needs_updated = 0;
                $logMessage = 'Request Updated in ILL by ' . user()->name;

                break;

            case 'NOT_AVAILABLE_FOR_PURCHASE':
                $status = Status::NOT_AVAILABLE_FOR_PURCHASE;
                if($requisition->ill_available == 1){
                    $logMessage = 'Request marked as Unavailable by ' . user()->name . '. This item should be requested via ILLiad.';
                }else{
                    $logMessage = 'Request marked as Unavailable by ' . user()->name . '. This item is unavailable for purchase or ILL.';
                }
                $requisition->ill_needs_updated = 1;

                // Nothing else needs to be done. ILL will borrow the book or cancel the request
                break;

            case 'PURCHASED':
                $status = Status::ORDERED;
                if($requisition->channel == Channel::ILL){
                    $requisition->ill_needs_updated = 1;
                }
                $logMessage = 'Request Purchased by ' . user()->name;

                // No need to send this for ILL. ILLiad will send the cancellation request with the reason.
                if($requisition->channel == Channel::PURCHASE){
                    $requisition->patron->notify(new PurchaseRequestApproved($requisition));
                }

                break;

            case 'RESEND':
                if($requisition->channel == 'ILL'){
                    $requisition->librarian->notify(new PendingILLRequest($requisition));
                }else{
                    $requisition->librarian->notify(new PendingPurchaseRequest($requisition));
                }

                $logMessage = 'Email resent to ' . $requisition->librarian->name . ' by ' . user()->name;

                break;

            default:
                $status = Status::PENDING_APPROVAL;
                $logMessage = 'Request set to Pending Approval by ' . user()->name;

        }

        if(!empty($status)){
            $requisition->status = $status;
        }

        if(!is_null($logMessage)){
            $requisition->messages()->save(new Message([
                'body' => $logMessage
            ]));
        }

        $requisition->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requisition $requisition)
    {
        $requisition->messages()->save(new Message([
            'body' => 'Request deleted by ' . user()->name,
        ]));

        $requisition->delete();

        return redirect()->route('admin.requests.index');
    }
}
