<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PurchaseRequestApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $patron;
    public $requisition;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($patron, $requisition)
    {
        $this->patron = $patron;
        $this->requisition = $requisition;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->replyTo('ill@byui.edu')
                ->markdown('mail.purchase-request-approved');
    }
}
