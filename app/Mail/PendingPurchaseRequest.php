<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PendingPurchaseRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $requisition;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $requisition)
    {
        $this->user = $user;
        $this->requisition = $requisition;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.pending-purchase-request');
    }
}
