<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PurchaseCancelled extends Mailable
{
    use Queueable, SerializesModels;

    public $patron;
    public $requisition;
    public $body;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($patron, $requisition, $body)
    {
        $this->patron = $patron;
        $this->requisition = $requisition;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->subject('Purchase Request Cancelled')
                ->replyTo([$this->requisition->librarian->email])
                ->markdown('mail.purchase-cancelled');
    }
}
