<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PullNotFound extends Mailable
{
    use Queueable, SerializesModels;

    public $patron;
    public $pull;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($patron, $pull)
    {
        $this->patron = $patron;
        $this->pull = $pull;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
                ->replyTo('ill@byui.edu')
                ->subject('Item Not Found')
                ->markdown('mail.pull-not-found');
    }
}
