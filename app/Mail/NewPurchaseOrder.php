<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewPurchaseOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $requisition;
    public $formats;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $requisition, $formats)
    {
        $this->user = $user;
        $this->requisition = $requisition;
        $this->formats = $formats;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.new-purchase-order')
                    ->replyTo('ill@byui.edu');
    }
}
