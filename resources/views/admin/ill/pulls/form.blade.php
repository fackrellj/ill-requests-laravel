{!! Form::fieldsetOpen('Patron Information') !!}
    {!! Form::hidden('id', $patron->id) !!}
    <div class="row">
        <div class="col col-md col-lg">
            {!! Form::text('name', 'Name', $patron->name)->attrs(['readonly' => true])->required() !!}
        </div>
        <div class="col col-md col-lg">
            {!! Form::text('email', 'Email', $patron->email)->attrs(['readonly' => true])->required() !!}
        </div>
        <div class="col col-md col-lg">
            {!! Form::text('inumber', 'I#', $patron->inumber)->attrs(['readonly' => true])->required() !!}
        </div>
    </div>

{!! Form::fieldsetClose() !!}

{!! Form::fieldsetOpen('Request Information') !!}

    <div class="row">
        <div class="col-5 col-md-5 col-lg-5">
            {!! Form::text('title', 'Title')->required(false) !!}
        </div>
        <div class="col-2 col-md-5 col-lg-5">
            {!! Form::text('author', 'Author')->required(false) !!}
        </div>
    </div>

    <div class="row">
        <div class="col-5 col-md-5 col-lg-5">
            {!! Form::text('location', 'Location')->required(false) !!}
        </div>
        <div class="col-2 col-md-5 col-lg-5">
            {!! Form::text('call_number', 'Call #')->required() !!}
        </div>
    </div>


{!! Form::fieldsetClose() !!}


@push('scripts')

@endpush
