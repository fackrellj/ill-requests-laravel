@extends('layouts.admin')

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Pull Requests</h1>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card mb-2">
                    <div class="card-body px-4 pt-1 pb-0">
                        <form action="{{ route('admin.pulls.index') }}" class="form" method="get">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="q">Search by Name or Title</label>
                                        <input type="search" class="form-control" id="q" name="q" value="{{ request()->get('q', '') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::select('status', 'Status', [
                                            'pending' => 'Pending',
                                            'pulled' => 'Pulled',
                                            'cancelled' => 'Cancelled',
                                            'all' => 'All',
                                        ],
                                        request()->get('status', 'pending'))->required(false) !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped table-md table-hover">
                                <tbody>
                                    <tr>
                                        <th>Patron Type</th>
                                        <th>Patron Name</th>
                                        <th>Request Date</th>
                                        <th>Title</th>
                                        <th>Author</th>
                                        <th>Location</th>
                                        <th>Call #</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    @foreach ($pulls as $pull)
                                        <tr style="cursor: pointer;" class="@if($pull->deleted_at) table-danger @elseif($pull->pulled) table-success @else @endif">
                                            <td onclick="javascript: window.location.href = '{{ route('admin.pulls.show', ['pull' => $pull]) }}'">
                                                {{--<a href="{{ route('admin.requests.show', ['request' => $requisition]) }}" style="padding: 6px 8px">--}}
                                                    {{ $pull->patron->affiliation }}
                                                {{--</a>--}}
                                            </td>
                                            <td onclick="javascript: window.location.href = '{{ route('admin.pulls.show', ['pull' => $pull]) }}'">
                                                {{ $pull->patron->last_first_name }}
                                            </td>
                                            <td onclick="javascript: window.location.href = '{{ route('admin.pulls.show', ['pull' => $pull]) }}'">
                                                {{ $pull->created_at->format('m-d-Y') }}
                                            </td>
                                            <td onclick="javascript: window.location.href = '{{ route('admin.pulls.show', ['pull' => $pull]) }}'">
                                                {{ $pull->title }}
                                            </td>
                                            <td onclick="javascript: window.location.href = '{{ route('admin.pulls.show', ['pull' => $pull]) }}'">
                                                {{ $pull->author }}
                                            </td>
                                            <td onclick="javascript: window.location.href = '{{ route('admin.pulls.show', ['pull' => $pull]) }}'">
                                                {{ $pull->location }}
                                            </td>
                                            <td onclick="javascript: window.location.href = '{{ route('admin.pulls.show', ['pull' => $pull]) }}'">
                                                {{ $pull->call_number }}
                                            </td>
                                            <td>
                                                {{--@if($pull->patron->affiliation == 'Employee'  && $pull->pulled != 1)@if($pull->patron->affiliation == 'Employee'  && $pull->pulled != 1)
                                                    {!! Form::open()->put()->route('admin.pulls.pulled', ['pull' => $pull]) !!}
                                                        {!! Form::submit("Pulled")->success()->attrs(['name' => "action", 'value' => "pulled"]) !!}
                                                    {!! Form::close()!!}
                                                @endif--}}
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.pulls.print', ['pull' => $pull]) }}" class="btn btn-dark" target="_blank" rel="noreferrer noopener">
                                                    Print
                                                </a>
                                            </td>
                                            <td>
                                                {!! Form::open()->get()->route('admin.pulls.edit', ['pull' => $pull]) !!}
                                                    {!! Form::submit("Edit")->info() !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-left">
                        <nav class="d-inline-block">
                            {!! $pulls->appends(request()->all())->links() !!}
                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection


@push('css')

@endpush

@push('scripts')
    <script src="{{ asset('assets/js/page/components-table.js') }}"></script>
    <script>
        $(function(){
            $(document).on('change', '#status', function(){
                $(this).closest('form').submit();
            });
        });
    </script>
    {{--<script>
        $(function(){
            $(document).on('click', 'a.btn-dark', function(e){
                e.stopPropagation();
            });
        });
    </script>--}}
@endpush


