<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
{{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/ionicons/css/ionicons.min.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">


    @stack('css')
    <style>
        table {
            font-size: 24px;
        }
    </style>
</head>
<body class="sidebar-mini" style="margin-top: 300px;">

        <table class="table table-striped">
            <tbody>
                <tr>
                    <th style="width: 20%;">Patron Name</th>
                    <td  style="width: 80%;">
                        {{ $pull->patron->last_first_name }}
                    </td>
                </tr>
                @if($pull->patron->affiliation == 'Employee')
                    <tr>
                        <th>Office</th>
                        <td>
                            {{ $pull->patron->office }}
                        </td>
                    </tr>
                @endif
                <tr>
                    <th>Patron Type</th>
                    <td>
                        {{ $pull->patron->affiliation }}
                    </td>
                </tr>
                <tr>
                    <th>Request Date</th>
                    <td>
                        {{ $pull->created_at->format('m-d-Y') }}
                    </td>
                </tr>
                <tr>
                    <th>Title</th>
                    <td>
                        {{ $pull->title }}
                    </td>
                </tr>
                <tr>
                    <th>Author</th>
                    <td>
                        {{ $pull->author }}
                    </td>
                </tr>
                <tr>
                    <th>Location</th>
                    <td>
                        {{ $pull->location }}
                    </td>
                </tr>
                <tr>
                    <th>Call #</th>
                    <td>
                        {{ $pull->call_number }}
                    </td>
                </tr>
            </tbody>
            <tfoot>

            </tfoot>
        </table>

    <!-- General JS Scripts -->
    <script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
    <script>
        $(function(){
            window.print();
        });
    </script>
</body>
</html>
