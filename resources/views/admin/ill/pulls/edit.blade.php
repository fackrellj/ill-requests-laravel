@extends('layouts.admin')

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Edit Pull Request</h1>
            <div class="section-header-breadcrumb">
                {!!Form::open()->attrs(['class' => 'delete-form'])->delete()->route('admin.pulls.destroy', ['pull' => $pull])!!}
                {!! Form::submit("Delete")->danger() !!}
                {!!Form::close()!!}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open()->fill($pull)->put()->route('admin.pulls.update', ['pull' => $pull]) !!}
                            @include('admin.ill.pulls.form')
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <button class="btn btn-primary btn-block" type="submit">SAVE</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@push('css')

@endpush

@push('scripts')
    <script>
        $(function(){

        })
    </script>
@endpush

