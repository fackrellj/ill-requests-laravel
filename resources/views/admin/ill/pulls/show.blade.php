@extends('layouts.admin')

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Edit Pull Request</h1>
            <div class="section-header-breadcrumb">
                {!!Form::open()->attrs(['class' => 'missing-form'])->put()->route('admin.pulls.missing', ['pull' => $pull])!!}
                    <button class="btn btn-info" type="submit" name="action" value="not-found">
                        <i class="fas fa-envelope"></i>
                        ITEM MISSING
                    </button>
                {!!Form::close()!!}
                {!!Form::open()->attrs(['class' => 'missing-form'])->put()->route('admin.pulls.missing', ['pull' => $pull])!!}
                    <button class="btn btn-outline-danger" type="submit" name="action" value="lost">
                        <i class="fas fa-envelope"></i>
                        CANCEL REQUEST
                    </button>
                {!!Form::close()!!}
                {!!Form::open()->attrs(['class' => 'delete-form'])->delete()->route('admin.pulls.destroy', ['pull' => $pull])!!}
                    {!! Form::submit("Delete")->danger() !!}
                {!!Form::close()!!}
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open()->fill($pull)->put()->route('admin.pulls.pulled', ['pull' => $pull]) !!}
                            @include('admin.ill.pulls.form', ['patron' => $pull->patron])
                            @if($pull->patron->affiliation != 'Employee')
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        {!! Form::text('barcode', 'Barcode')->attrs(['autofocus' => true])->required(false) !!}
                                    </div>
                                </div>
                            @endif
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-12 col-md-12 col-lg-12">
                                        <button class="btn btn-primary btn-block" type="submit" name="action" value="pulled">MARK PULLED</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            {{--<div class="col-lg-4 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Recent Activities</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled list-unstyled-border">
                            @foreach($pull->messages as $message)
                                <li class="media">
                                    @if($message->messageable_id)
                                        <i class="far fa-sticky-note mr-3 rounded-circle" style="width: 50px; height: 50px;"></i>
                                    @else
                                        <i class="fas fa-cogs mr-3 rounded-circle" style="width: 50px; height: 50px;"></i>
                                    @endif
                                    <div class="media-body">
                                        <div class="float-right text-primary">{{ $message->created_at->toDayDateTimeString() }}</div>
                                        @if($message->messageable_id)
                                            <div class="media-title">{{ $message->messageable->name }}</div>
                                        @else
                                            <div class="media-title">System</div>
                                        @endif
                                        <span class="text-small text-muted">
                                            {!! $message->body !!}
                                        </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        {!! Form::open()->post()->route('admin.pulls.messages.add', ['pull' => $pull]) !!}
                            <textarea name="body" class="summernote-simple"></textarea>
                            <button class="btn btn-block btn-primary" type="submit">ADD NOTE</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>--}}
        </div>
    </section>

@endsection


@push('css')
    <style>
        .missing-form{
            margin-right: 12px;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(function(){

        })
    </script>
@endpush

