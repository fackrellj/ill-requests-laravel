@extends('layouts.admin')

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Create New Request</h1>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open()->fill($requisition)->id('requisition-approval')->post()->route('admin.requests.store') !!}
                            @include('admin.ill.requisitions.form')
                            <div class="card-footer">
                                <div class="row">
                                    <button class="btn btn-primary btn-block" type="submit">SAVE</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            {{--<div class="col-lg-4 col-md-12 col-12 col-sm-12">
                @if($pendingRequests->count() > 0)
                    <div class="card">
                        <div class="card-header">
                            Pending Requests
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled list-unstyled-border">
                                @foreach($pendingRequests as $key => $pendingRequest)
                                <li class="media pending-request" data-id="{{ $key }}">
                                    <div class="media-body">
                                        <div class="media-right">{{ $pendingRequest->TransactionNumber }}</div>
                                        <div class="media-title">{{ $pendingRequest->LoanTitle }}</div>
                                        <div class="text-muted text-small">by {{ $pendingRequest->LoanAuthor }}</div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
            </div>--}}
        </div>
        {!! Form::open()->id('search-patrons-form')->get()->route('admin.requests.create') !!}
            {!! Form::hidden('tn_number', '') !!}
        {!! Form::close() !!}
    </section>

@endsection


@push('css')

@endpush

@push('scripts')
    <script>
        $(function(){
            $(document).on('click', '#search-patrons', function(e){
                e.preventDefault();
                var $button = $(this);
                var $form = $('#search-patrons-form');
                var $tn = $form.find('#tn_number');
                $tn.val($('#requisition-approval').find('#tn_number').val());
                $form.submit();
            });
        })
    </script>
@endpush

