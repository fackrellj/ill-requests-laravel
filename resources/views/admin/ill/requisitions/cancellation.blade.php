@extends('layouts.admin')

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Cancel Purchase Request</h1>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open()->fill($requisition)->id('requisition-cancellation')->post()->route('admin.requests.cancel', ['request' => $requisition]) !!}

                            <div class="row">
                                <div class="col col-md col-lg">
                                    {!! Form::select('cancellation', 'Reason', ([null => '-- Select one --'] + $reasons), request('reason'))->required(true) !!}
                                </div>
                            </div>

                            @if($reason)
                                <div class="row">
                                    <div class="col col-md col-lg">
                                        {!! Form::textarea('body', 'Email Message', $reason->description)->required(false)->attrs(['class' => 'summernote']) !!}
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <button class="btn btn-primary btn-block" type="submit">SEND</button>
                                    </div>
                                </div>
                            @endif

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </section>

@endsection


@push('css')

@endpush

@push('scripts')
    <script>
        $(function(){
            $(document).on('change', '#cancellation', function(e){
                e.preventDefault();
                window.location = '{{ route('admin.requests.cancellation', ['request' => $requisition]) }}?reason=' + $(this).val();
            });
        })
    </script>
@endpush

