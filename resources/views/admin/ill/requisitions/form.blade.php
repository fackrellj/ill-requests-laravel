{!! Form::fieldsetOpen('Request Information') !!}

    <div class="row">
        @if($requisition->channel == \App\Enums\Channel::ILL)
            <div class="col-4 col-md-4 col-lg-4">
                <div class="row">
                    <div class="col col-md-8 col-lg-8">
                        {!! Form::text('tn_number', 'TN Number')->required(($requisition->channel == \App\Enums\Channel::ILL)) !!}
                    </div>
                    <div class="col col-md-4 col-lg-4">
                        <label for="search-patrons" style="color: #ffffff;">SEARCH</label>
                        <span class="btn btn-info" id="search-patrons">SEARCH</span>
                    </div>
                </div>
            </div>
        @endif
        <div class="col-4 col-md-4 col-lg-4">
            {!! Form::text('title', 'Title')->required() !!}
        </div>
        <div class="col-4 col-md-4 col-lg-4">
            {!! Form::text('author', 'Author')->required() !!}
        </div>
    </div>
    @if($requisition->channel == \App\Enums\Channel::ILL)
        <div class="row">
            <div class="col-12 col-md-4 col-lg-4">
                {!! Form::checkbox('ill_available', 'ILL Not Available', 0)->checked($requisition->ill_available === 0)->required(false) !!}
            </div>
            <div class="col-12 col-md-5 col-lg-5">
                <a href="http://www.amazon.com/s/?tag=mcklib-20&field-keywords=" class="amazon-search" target="_blank" title="Click to search Amazon for this title" style="@if($requisition->title) @else display: none; @endif">
                    <img src="/img/amazon.png" style="height: 25px; width: auto;"/>
                </a>
                <a href="http://search.ebscohost.com/login.aspx?authtype=ip,guest&custid=s8406107&groupid=main&profile=eds&direct=true&scope=site&bquery=" class="eds-search" target="_blank" title="Click to search Catalog for this title" style="@if($requisition->title) @else display: none; @endif">
                    <img src="/img/byui.jpg" style="height: 25px; width: auto;"/>
                </a>
            </div>
        </div>
        <br />
        <div class="row " @if(!($requisition->ill_available === 0)) style="display: none;" @endif>
            <div class="col-12 col-md-7 col-lg-7">
                <textarea name="ill_note" class="summernote-simple">{{ $requisition->ill_note }}</textarea>
            </div>
        </div>
    @endif

{!! Form::fieldsetClose() !!}

{!! Form::fieldsetOpen('Patron Information') !!}
{!! Form::hidden('id', $patron->id) !!}
{!! Form::hidden('inumber', $patron->inumber) !!}
{!! Form::hidden('roles', implode('|', $patron->roles)) !!}
<div class="row">
    <div class="col col-md col-lg">
        {!! Form::text('username', 'Username', $patron->username)->required() !!}
    </div>
    <div class="col col-md col-lg">
        {!! Form::text('name', 'Name', $patron->name)->required() !!}
    </div>
    <div class="col col-md col-lg">
        {!! Form::text('email', 'Email', $patron->email)->required() !!}
    </div>
</div>

{!! Form::fieldsetClose() !!}

{!! Form::fieldsetOpen('Availability  <span class="btn btn-sm btn-info" id="add-format" style="margin-left: 10px;"><i class="fas fa-plus"></i> Add Format</span>') !!}

    <div id="format-availability">
        @if($requisition->formats->count() > 0)
            @foreach($requisition->formats as $format)
                <div class="format-details">
                    <div class="row">
                        <div class="col col-md col-lg">
                            {!! Form::select('type[]', 'Format', ([null => '-- Select one --'] + \App\Enums\Format::namesAndKeys()), $format->type)->required(true) !!}
                        </div>
                        <div class="col col-md col-lg">
                            {!! Form::text('isbn[]', 'ISBN', $format->isbn)->required(false) !!}
                        </div>
                        <div class="col col-md col-lg">
                            {!! Form::text('edition[]', 'Edition', $format->edition)->required(false) !!}
                        </div>
                        <div class="col col-md col-lg">
                            {!! Form::text('cost[]', 'Cost' . ((! user()->hasRole('ILL Employee')?' (<span style="color: red; font-size: 10px;">required</span>)':'')),$format->cost)->required(! user()->hasRole('ILL Employee')) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md col-lg">
                            {!! Form::text('link[]', 'Link' . ((! user()->hasRole('ILL Employee')?' (<span style="color: red; font-size: 10px;">required</span>)':'')), $format->link)->required(! user()->hasRole('ILL Employee')) !!}
                            {!! Form::textarea('notes[]', 'Notes', $format->notes)->required(false) !!}
                        </div>
                    </div>
                    <span id="remove-format" class="btn btn-sm btn-danger">Remove</span>
                </div>
                @if(! $loop->last)
                    <hr />
                    <br />
                @endif
            @endforeach
        @endif
    </div>


{!! Form::fieldsetClose() !!}

{!! Form::fieldsetOpen('Assignment') !!}

<div class="row">
    <div class="col-5 col-md-5 col-lg-5">
        {!! Form::select('subject_id', 'Subject', $subjects)->required(false) !!}
    </div>
    <div class="col-5 col-md-5 col-lg-5">
        {!! Form::select('librarian_id', 'Librarian', $librarians)->required(false) !!}
    </div>
</div>

{!! Form::fieldsetClose() !!}


@push('scripts')
    <script id="format-template" type="text/html">
        <div class="format-details">
            <div class="row">
                <div class="col col-md col-lg">
                    {!! Form::select('type[]', 'Format', ([null => '-- Select one --'] + \App\Enums\Format::namesAndKeys()))->required(true) !!}
                </div>
                <div class="col col-md col-lg">
                    {!! Form::text('isbn[]', 'ISBN')->required(false) !!}
                </div>
                <div class="col col-md col-lg">
                    {!! Form::text('edition[]', 'Edition')->required(false) !!}
                </div>
                <div class="col col-md col-lg">
                    {!! Form::text('cost[]', 'Cost' . ((! user()->hasRole('ILL Employee')?' (<span style="color: red; font-size: 10px;">required</span>)':'')))->required(! user()->hasRole('ILL Employee')) !!}
                </div>
            </div>
            <div class="row">
                <div class="col col-md col-lg">
                    {!! Form::text('link[]', 'Link' . ((! user()->hasRole('ILL Employee')?' (<span style="color: red; font-size: 10px;">required</span>)':'')))->required(! user()->hasRole('ILL Employee')) !!}
                    {!! Form::textarea('notes[]', 'Notes')->required(false) !!}
                </div>
            </div>
        </div>
    </script>
    <script>
        var template = $('#format-template').html();
        var librarianSubjects = @json($librarianSubjects);
        $(function(){
            @if($requisition->formats->count() < 1)
                //$('#format-availability').append(template);
            @endif
            $(document).on('click', '#add-format', function(){
                $('#format-availability').append(template);
                $('#format-availability').find('.format-details').last().prepend('<hr />');
                $('#format-availability').find('.format-details').last().append('<span id="remove-format" class="btn btn-sm btn-danger">Remove</span>');
            });

            $(document).on('click', '#remove-format', function(){
                var $button = $(this);
                $button.closest('.format-details').remove();
            });

            $(document).on('change', '#subject_id', function(){
                var $subject = $(this);
                var $librarian = $('#librarian_id');
                $librarian.val(librarianSubjects[$subject.val()]);
            });

            $(document).on('click', '.amazon-search', function(e){
                e.preventDefault();
                var $link = $(this);
                window.open($link.attr('href') + $('input[name="title"]').val());
            });

            $(document).on('click', '.eds-search', function(e){
                e.preventDefault();
                var $link = $(this);
                window.open($link.attr('href') + $('input[name="title"]').val());
            });

            $(document).on('keyup', 'input[name="title"]', function(e){
                var $input = $(this);
                if($input.val().length > 0){
                    $('.amazon-search').show();
                    $('.eds-search').show();
                }else{
                    $('.amazon-search').hide();
                    $('.eds-search').hide();
                }

            });

        })
    </script>

    <script>
        $(function(){
            $(document).on('change', 'input[name="ill_available"]', function(){
                var checkbox = this;
                var $noteContainer = $('textarea[name="ill_note"]').closest('.row');
                if(checkbox.checked){
                    $noteContainer.show();
                }else{
                    $noteContainer.hide();
                }
            });
        });
    </script>
@endpush
