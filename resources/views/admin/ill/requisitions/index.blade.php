@extends('layouts.admin')

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Requests</h1>
        </div>

        <div class="row">
            <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                <a href="{{ route('admin.home', ['status' => \App\Enums\Status::NEW_PURCHASE_REQUEST]) }}" class="card card-statistic-1 mb-2">
                    <div class="card-icon bg-success">
                        <i class="fas fa-concierge-bell"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>New Purchase Request</h4>
                        </div>
                        <div class="card-body">
                            {{ $stats['new_purchase_request'] }}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                <a href="{{ route('admin.pulls.index') }}" class="card card-statistic-1 mb-2">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-truck"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Pull Requests</h4>
                        </div>
                        <div class="card-body">
                            {{ $stats['new_pull_request'] }}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                <a href="{{ route('admin.home', ['status' => \App\Enums\Status::PENDING_APPROVAL, 'state' => 'overdue']) }}"  class="card card-statistic-1 mb-2">
                    <div class="card-icon bg-danger">
                        <i class="far fa-clock"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Expired Approval</h4>
                        </div>
                        <div class="card-body">
                            {{ $stats['overdue'] }}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                <a href="{{ route('admin.home', ['status' => \App\Enums\Status::APPROVED_FOR_ILL]) }}"  class="card card-statistic-1 mb-2">
                    <div class="card-icon bg-info">
                        <i class="far fa-share-square"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Ill Approved</h4>
                        </div>
                        <div class="card-body">
                            {{ $stats['ill_approved'] }}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                <a href="{{ route('admin.home', ['status' => \App\Enums\Status::ORDERED]) }}"  class="card card-statistic-1 mb-2">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-shopping-cart"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Ordered</h4>
                        </div>
                        <div class="card-body">
                            {{ $stats['ordered'] }}
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 col-12">
                <a href="{{ route('admin.home', ['status' => \App\Enums\Status::CANCELLED]) }}"  class="card card-statistic-1 mb-2">
                    <div class="card-icon bg-secondary">
                        <i class="fas fa-ban"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Cancelled</h4>
                        </div>
                        <div class="card-body">
                            {{ $stats['cancelled'] }}
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card mb-2">
                    <div class="card-body px-4 pt-1 pb-0">
                        <form action="{{ route('admin.requests.index') }}" class="form" method="get">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="q">Search by Name, TN Number, or Title</label>
                                        <input type="search" class="form-control" id="q" name="q" value="{{ request()->get('q', '') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    {!! Form::select('status', 'Status', $statuses, request()->get('status', null))->required(false) !!}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped table-md table-hover">
                                <tbody>
                                    <tr>
                                        <th>TN Number</th>
                                        <th>Status</th>
                                        <th>Date Requested</th>
                                        <th>Title</th>
                                        <th>Librarian</th>
                                        <th>Patron</th>
                                        <th>Type</th>
                                    </tr>
                                    @foreach ($requisitions as $requisition)
                                        <tr @if($requisition->ill_needs_updated == 1) class="table-warning" @elseif($requisition->status == \App\Enums\Status::NEW_PURCHASE_REQUEST) class="table-danger" @endif style="cursor: pointer;" onclick="javascript: window.location.href = '{{ route('admin.requests.show', ['request' => $requisition]) }}'">
                                            <td>
                                                {{--<a href="{{ route('admin.requests.show', ['request' => $requisition]) }}" style="padding: 6px 8px">--}}
                                                    {{ $requisition->display_tn_number }}
                                                {{--</a>--}}
                                            </td>
                                            <td>
                                                @if($requisition->librarian_added_note)
                                                    <i class="far fa-sticky-note text-info"></i>
                                                @endif
                                                {{ \App\Enums\Status::valueForKey($requisition->status) }}
                                            </td>
                                            <td>
                                                {{ $requisition->created_at->format('m-d-Y') }}
                                            </td>
                                            <td>
                                                {{--<a href="{{route('admin.requests.show', ['request' => $requisition]) }}" style="padding: 6px 8px">--}}
                                                    {{ $requisition->title }}
                                                {{--</a>--}}
                                            </td>
                                            <td>
                                                @if($requisition->librarian)
                                                    {{ $requisition->librarian->name }}
                                                @endif
                                            </td>
                                            <td>
                                                {{ $requisition->patron->name }}
                                            </td>
                                            <td>
                                                {{ $requisition->patron->affiliation }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-left">
                        <nav class="d-inline-block">
                            {!! $requisitions->appends(request()->all())->links() !!}
                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection


@push('css')

@endpush

@push('scripts')
    <script src="{{ asset('assets/js/page/components-table.js') }}"></script>
    <script>
        $(function(){
            $(document).on('change', '#status', function(){
                $(this).closest('form').submit();
            });
        });
    </script>
@endpush

