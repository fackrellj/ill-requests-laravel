@extends('layouts.admin')

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Edit Request</h1>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open()->fill($requisition)->id('requisition-approval')->put()->route('admin.requests.update', ['request' => $requisition]) !!}
                            @include('admin.ill.requisitions.form')
                            <div class="card-footer">
                                <div class="row">
                                    <button class="btn btn-primary btn-block" type="submit">SAVE</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@push('css')

@endpush

@push('scripts')
    <script>
        $(function(){

        })
    </script>
@endpush

