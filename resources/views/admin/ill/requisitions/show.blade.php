@extends('layouts.admin')

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>{{ $requisition->display_tn_number }}</h1>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    {!! Form::open()->id('requisition-approval')->post()->route('admin.requests.approve', ['request' => $requisition]) !!}
                        <div class="card-header">
                            <h4>{{ $requisition->title }} <small>by {{ $requisition->author }}</small></h4>
                            <div class="card-header-action">
                                <a href="{{ route('admin.requests.edit', ['request' => $requisition]) }}" class="btn btn-info">
                                    Edit
                                </a>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped table-md">
                                    <thead>
                                    <tr>
                                        <th>Format</th>
                                        <th>Edition</th>
                                        <th>Cost</th>
                                        <th>Notes</th>
                                        <th>ISBN</th>
                                        <th>Link</th>
                                        <th>Selection</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($requisition->formats as $format)
                                            <tr @if($format->approval) class="table-success" @endif>
                                                <td>
                                                    {{ (($format->type)?\App\Enums\Format::valueForKey($format->type):'') }}
                                                </td>
                                                <td>
                                                    {{ $format->edition }}
                                                </td>
                                                <td>
                                                    @if(! empty($format->cost))
                                                        ${{ $format->cost }}
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $format->notes }}
                                                </td>
                                                <td>
                                                    {{ $format->isbn }}
                                                    @if($format->isbn)
                                                        <br />
                                                        <span>
                                                            <a href="http://www.amazon.com/s/?tag=mcklib-20&field-keywords={{ $format->isbn }}" target="_blank" title="Click to search Amazon for this ISBN">
                                                                <img src="/img/amazon.png" style="height: 25px; width: auto;"/>
                                                            </a>
                                                            <a href="https://byui.ent.sirsi.net/client/en_US/main/search/results?te=ILS&qu=ISBN%3D{{ $format->isbn }}" target="_blank" title="Click to search our catalog for this ISBN">
                                                                <img src="/img/byui.jpg" style="height: 25px; width: auto;"/>
                                                            </a>
                                                        </span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($format->link)
                                                        <a href="{{ $format->link }}" class="btn btn-sm btn-warning" target="_blank">View</a>
                                                    @endif
                                                </td>
                                                <td style="text-align: center;">
                                                    <div class="form-check">
                                                        <input name="format[]" class="form-check-input" type="checkbox" id="format-{{ $format->id }}" value="{{ $format->id }}" @if($requisition->formats->count() ==1) checked @endif>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>

                            <br />

                            <div class="table-responsive">
                                <table class="table table-striped table-md">
                                    <tbody>
                                        <tr>
                                            <th>Status: </th>
                                            <td>
                                                {{ \App\Enums\Status::valueForKey($requisition->status) }}
                                            </td>
                                            <th></th>
                                            <td>

                                            </td>
                                        </tr>
                                        @if($requisition->approved_at)
                                            <tr>
                                                <th>Approved At: </th>
                                                <td>
                                                    {{ $requisition->approved_at->toDayDateTimeString() }}
                                                </td>
                                                <th></th>
                                                <td>

                                                </td>
                                            </tr>
                                        @endif
                                        @if($requisition->purchase_request_sent_at)
                                            <tr>
                                                <th>Sent to Purchasing At: </th>
                                                <td>
                                                    {{ $requisition->purchase_request_sent_at->toDayDateTimeString() }}
                                                </td>
                                                <th></th>
                                                <td>

                                                </td>
                                            </tr>
                                        @endif
                                        @if($requisition->received_at)
                                            <tr>
                                                <th>Received At: </th>
                                                <td>
                                                    {{ $requisition->received_at->toDayDateTimeString() }}
                                                </td>
                                                <th></th>
                                                <td>

                                                </td>
                                            </tr>
                                        @endif
                                        <tr class="table-info">
                                            <th>Subject Area: </th>
                                            <td>
                                                {{ optional($requisition->subject)->name }}
                                            </td>
                                            <th>Assigned Librarian: </th>
                                            <td>
                                                {{ optional($requisition->librarian)->name }}
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                @hasanyrole('Super|Admin|Librarian')
                                    @if($requisition->status == \App\Enums\Status::PENDING_APPROVAL)
                                        @if($requisition->formats->count() > 0)
                                            <div class="col-lg-4 col-md-4 col-4 col-sm-4" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                <button name="action" class="btn btn-primary btn-block" type="submit" value="BUY">BUY</button>
                                            </div>
                                            @if($requisition->channel == \App\Enums\Channel::PURCHASE)
                                                <div class="col-lg-4 col-md-4 col-4 col-sm-4" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                    <button name="action" class="btn btn-block btn-secondary" type="submit" value="CANCEL">CANCEL</button>
                                                </div>
                                            @endif
                                        @else
                                            <p>You must add a format to have the option to purchase.</p>
                                        @endif

                                        @if($requisition->channel == \App\Enums\Channel::ILL)
                                            @if($requisition->ill_available === 0)
                                                <div class="col-lg-4 col-md-4 col-4 col-sm-4" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                    <button name="action" class="btn btn-block btn-secondary" type="submit" value="CANCEL">CANCEL</button>
                                                </div>
                                            @else
                                                <div class="col-lg-4 col-md-4 col-4 col-sm-4" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                    <button name="action" class="btn btn-info btn-block" type="submit" value="BORROW">BORROW</button>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-4 col-sm-4" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                    {!! $requisition->ill_note !!}
                                                </div>
                                            @endif
                                        @endif
                                    @elseif($requisition->status != \App\Enums\Status::ORDERED && $requisition->status != \App\Enums\Status::RECEIVED && $requisition->status != \App\Enums\Status::NEW_PURCHASE_REQUEST)
                                        <div class="col-lg-4 col-md-4 col-4 col-sm-12" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                            <button name="action" class="btn btn-primary btn-block" type="submit" value="RESET">CHANGE APPROVAL</button>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-4 col-sm-12" style="">
                                            <p>The system does not notify others of changes to a request. You may need to contact an individual to cancel a Purchase or ILL request once it has been created.</p>
                                        </div>
                                    @endif
                                @else
                                    @hasrole('ILL Employee')
                                        @if($requisition->ill_needs_updated == 1)
                                            <div class="col-lg-4 col-md-4 col-4 col-sm-4" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                <button name="action" class="btn btn-warning btn-block" type="submit" value="ILL_UPDATED">ILLiad UPDATED</button>
                                            </div>
                                        @endif
                                        @if($requisition->status == \App\Enums\Status::ORDERED)
                                            <div class="col-lg-4 col-md-4 col-4 col-sm-4" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                <button name="action" class="btn btn-primary btn-block" type="submit" value="RECEIVED">RECEIVED</button>
                                            </div>
                                        @endif
                                        @if($requisition->status == \App\Enums\Status::PENDING_APPROVAL)
                                            <div class="col-lg-4 col-md-4 col-4 col-sm-4" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                <button name="action" class="btn btn-info btn-block" type="submit" value="RESEND">RESEND</button>
                                            </div>
                                        @endif
                                    @endhasrole
                                    @hasrole('ILL Purchasing')
                                        @if($requisition->status == \App\Enums\Status::APPROVED_FOR_PURCHASE)
                                            <div class="col-lg-4 col-md-4 col-4 col-sm-4" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                <button name="action" class="btn btn-primary btn-block" type="submit" value="PURCHASED">PURCHASED</button>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-6 col-sm-6" style="text-align: center; padding-left: 30px; padding-right: 30px;">
                                                <button name="action" class="btn btn-secondary btn-block" type="submit" value="NOT_AVAILABLE_FOR_PURCHASE">NOT AVAILABLE FOR PURCHASE</button>
                                            </div>
                                        @endif
                                    @endhasrole
                                @endunlessrole
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>

                <div class="card author-box card-primary">
                    <div class="card-body">
                        <div class="author-box-left">
                            <img alt="" src="{{ $requisition->patron->avatar }}" class="rounded-circle author-box-picture">
                            <div class="clearfix"></div>
                        </div>
                        <div class="author-box-details">
                            <div class="author-box-name">
                                {{ $requisition->patron->name }}
                            </div>
                            <div class="author-box-job">{{ implode(" | ", $requisition->patron->roles) }}</div>
                            <div class="author-box-description">
                                <p>
                                    <a href="mailto:{{ $requisition->patron->email }}">{{ $requisition->patron->email }}</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped table-md">
                                <tbody>
                                <tr>
                                    <th>Date Requested</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                </tr>
                                @foreach ($requisition->patron->requisitions as $pastreq)
                                    @if($requisition->id != $pastreq->id)
                                        <tr>
                                            <td>
                                                {{ $pastreq->created_at->format('m-d-Y') }}
                                            </td>
                                            <td>
                                                <a href="{{route('admin.requests.show', ['request' => $pastreq]) }}" style="padding: 6px 8px">{{ $pastreq->title }} </a>
                                            </td>
                                            <td>
                                                {{ $pastreq->author }}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                @hasrole('ILL Employee')
                    {!! Form::open()->attrs(['class' => 'delete-form'])->delete()->route('admin.requests.destroy', ['request' => $requisition]) !!}
                        {!! Form::submit("Delete")->attrs(['style' => 'margin-top: 20px'])->danger() !!}
                    {!! Form::close() !!}
                @endhasrole
            </div>
            <div class="col-lg-4 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Recent Activities</h4>
                        <div class="card-header-action">
                            @hasrole('ILL Employee')
                                @if($requisition->librarian_added_note)
                                    {!! Form::open()->attrs([])->post()->route('admin.requests.messages.read', ['request' => $requisition]) !!}
                                        {!! Form::submit("Mark Read")->attrs(['style' => 'margin-top: 0px'])->info() !!}
                                    {!! Form::close() !!}
                                @endif
                            @endhasrole
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled list-unstyled-border">
                            @foreach($requisition->messages as $message)
                                <li class="media">
                                    @if($message->messageable_id)
                                        <i class="far fa-sticky-note mr-3 rounded-circle" style="width: 50px; height: 50px;"></i>
                                    @else
                                        <i class="fas fa-cogs mr-3 rounded-circle" style="width: 50px; height: 50px;"></i>
                                    @endif
                                    <div class="media-body">
                                        <div class="float-right text-primary">{{ $message->created_at->toDayDateTimeString() }}</div>
                                        @if($message->messageable_id)
                                            <div class="media-title">{{ $message->messageable->name }}</div>
                                        @else
                                            <div class="media-title">System</div>
                                        @endif
                                        <span class="text-small text-muted">
                                            {!! $message->body !!}
                                        </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        {!! Form::open()->post()->route('admin.requests.messages.add', ['request' => $requisition]) !!}
                            <textarea name="body" class="summernote-simple"></textarea>
                            <button class="btn btn-block btn-primary" type="submit">ADD NOTE</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@push('css')

@endpush

@push('scripts')
    <script>
        $(function(){
            $(document).on('click', 'form#requisition-approval button[type=submit]', function(){
                $("button[type=submit]", $(this).parents("form")).removeAttr("clicked");
                $(this).attr("clicked", "true");
            });

            $(document).on('submit', '#requisition-approval', function(){
                var checkedOne = true;
                @if($requisition->channel != \App\Enums\Channel::ILL)
                    var action = $("form#requisition-approval button[type=submit][clicked=true]").val();
                    if((action != 'CANCEL') && (action != 'ILL_UPDATED') && (action != 'RECEIVED') && (action != 'BORROW') && (action != 'RESEND')) {
                        var checkboxes = document.querySelectorAll('input[name="format[]"]');
                        var checkedOne = Array.prototype.slice.call(checkboxes).some(x => x.checked);
                        if (checkedOne == false) {
                            alert("Please select the item you are approving.");
                        }
                    }
                @endif
                return checkedOne;
            });
        })
    </script>
@endpush

