

{!! Form::text('name', 'Name')->required() !!}
{!! Form::text('username', 'Username')->required() !!}

<div class="row">
    <div class="col">
        {!! Form::text('email', 'Email')->required() !!}
    </div>
    <div class="col" style="padding-top: 38px;">
        {!! Form::checkbox('receive_purchase_notifications', 'Receive Purchase Notifications', 1)->required(false) !!}
    </div>
</div>

<div class="row">
    <div class="col">
        {!! Form::select('role', 'Role', $roles->pluck('name', 'name')->all(), $user->getRoleNames()->first() )->required() !!}
    </div>
</div>

<div class="clearfix">&nbsp;</div>



@push('css')
    <style>

    </style>
@endpush
