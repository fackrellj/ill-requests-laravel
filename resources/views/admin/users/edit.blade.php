@extends('layouts.admin')

@section('page_name', 'Editing' )

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Editing {{ $user->name }}</h1>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="card">
                    <div class="card-body">
                        {!!Form::open()->fill($user)->patch()->route('admin.users.update', ['user' => $user->id])!!}

                            @include('admin.users.form')

                            {!!Form::submit("Update")!!}

                        {!!Form::close()!!}
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-inline-block">

                            {!!Form::open()->attrs(['class' => 'delete-form'])->delete()->route('admin.users.destroy', ['user' => $user])!!}
                                {!! Form::submit("Delete")->danger() !!}
                            {!!Form::close()!!}

                        </nav>
                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection


@push('css')

@endpush

@push('scripts')

@endpush
