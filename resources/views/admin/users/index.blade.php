@extends('layouts.admin')

@section('page_name', 'Subjects' )

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Users</h1>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped table-md">
                                <tbody><tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Roles</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                @foreach ($users as $user)
                                    <tr>
                                        <td></td>
                                        <td>
                                            <a href="{{route('admin.users.edit', ['user' => $user]) }}" style="padding: 6px 8px">{{ $user->name }} </a>
                                        </td>
                                        <td>
                                            {{ $user->getRoleNames()->join(', ') }}
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody></table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h4>New User</h4>
                    </div>
                    <div class="card-body">
                        {!!Form::open()->post()->route('admin.users.store')!!}

                        {!!Form::text('name', 'Name')->required()!!}
                        {!!Form::text('username', 'Username')->required()!!}
                        {!!Form::text('email', 'Email')->required()!!}
                        <div style="padding-top: 10px;">
                            {!! Form::checkbox('receive_purchase_notifications', 'Receive Purchase Notifications', 1)->checked(false)->required(false) !!}
                        </div>
                        <br />
                        {!!Form::submit("Save")!!}

                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@push('css')

@endpush

@push('scripts')
    <script src="{{ asset('assets/js/page/components-table.js') }}"></script>
@endpush
