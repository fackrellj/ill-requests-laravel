@extends('layouts.admin')

@section('page_name', 'Editing' )

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Editing Assigned Subjects</h1>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="card">
                    <div class="card-body">
                        {!!Form::open()->put()->route('admin.users.subjects.update')!!}

                            {!!Form::submit("Update")!!}

                            <br />
                            <br />

                            @foreach($subjects as $subject)

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">{{ $subject->name }}</label>
                                    <div class="col-sm-9">
                                        <input type="hidden" id="subjects_{{ $subject->id }}" name="subjects[]" value="{{ $subject->id }}">
                                        {!! Form::select('librarians[]', null, ([null => '-- Select one --'] + $users->pluck('name', 'id')->all()), optional($subject->librarian)->user_id)->required(false) !!}
                                    </div>
                                </div>

                            @endforeach

                            {!!Form::submit("Update")!!}

                        {!!Form::close()!!}
                    </div>
                    <div class="card-footer text-right">

                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection


@push('css')

@endpush

@push('scripts')

@endpush
