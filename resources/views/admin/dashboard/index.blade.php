@extends('layouts.admin')

@section('content')


    <section class="section">
        <div class="section-header">
            <h1>Stats Dashboard</h1>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card mb-2">
                    <div class="card-body px-4 pt-1 pb-0">
                        {!! $pullRequestsByUserType->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection


@push('css')

@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>

    {!! $pullRequestsByUserType->script() !!}
@endpush


