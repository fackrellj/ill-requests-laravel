<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">ILL MANAGER</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="#">ILL</a>
        </div>
        <ul class="sidebar-menu">

            <li class="{{ request()->is('admin/home') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.home') }}">
                    <i class="fas fa-tasks"></i> <span>Home</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/requests/create') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.requests.create') }}">
                    <i class="fas fa-plus"></i> <span>New Request</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/pulls') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.pulls.index') }}">
                    <i class="fas fa-truck"></i> <span>Pulls</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/dashboard') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('admin.dashboard') }}">
                    <i class="fas fa-chart-bar"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cogs"></i> <span>System</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ request()->is('admin/users*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('admin.users.index') }}">Users</a></li>
                    <li class="{{ request()->is('admin/users/subjects*') ? 'active' : '' }}"><a class="nav-link" href="{{ route('admin.users.subjects.edit') }}">Assigned Subjects</a></li>
                </ul>
            </li>
            <li>
                <a class="nav-link" href="/telescope" target="_blank">
                    <i class="fas fa-bug"></i> <span>Telescope</span>
                </a>
            </li>

            @role('Super')
                <li>
                    <a class="nav-link" href="/horizon" target="_blank">
                        <i class="fas fa-user-md"></i> <span>Horizon</span>
                    </a>
                </li>
            @endrole
        </ul>
        @role('Super')
            <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                <a href="https://demo.getstisla.com/index.html" class="btn btn-primary btn-lg btn-block btn-icon-split" target="_blank">
                    <i class="fas fa-rocket"></i> Documentation
                </a>
            </div>
        @endrole
    </aside>
</div>
