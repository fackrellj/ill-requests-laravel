@if(auth()->check())

@if(!empty($stats['messages']))
    <li class="item">
        <a href="{{ route('admin.home', ['messages' => 1]) }}" class="nav-link nav-link-lg @if($stats['messages'] > 0) beep @endif">
            <i class="far fa-sticky-note"></i>
        </a>
    </li>
@endif

<li class="dropdown dropdown-list-toggle">
    <a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg @if(auth()->user()->notifications->count() > 0) beep @endif ">
        <i class="far fa-bell"></i>
    </a>
    <div class="dropdown-menu dropdown-list dropdown-menu-right">
        <div class="dropdown-header">Notifications
            <div class="float-right">
                <a href="#">Mark All As Read</a>
            </div>
        </div>
        <div class="dropdown-list-content dropdown-list-icons">
            {{--@foreach(auth()->user()->notifications as $notification)
                <a href="#" class="dropdown-item dropdown-item-unread">
                    <div class="dropdown-item-icon bg-primary text-white">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="dropdown-item-desc">
                        {{ $notification->data['message'] }}
                        <div class="time text-primary">{{ $notification->created_at->diffForHumans() }}</div>
                    </div>
                </a>
            @endforeach--}}
        </div>
        {{--<div class="dropdown-footer text-center">
            <a href="#">View All <i class="fas fa-chevron-right"></i></a>
        </div>--}}
    </div>
</li>
@endif
