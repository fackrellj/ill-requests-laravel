@component('mail::message')
# Hi,

<p>
{!! $message->body !!}
</p>

<br>
@component('mail::table')
|        |        |
| :------------- | -------------: |
| Title      | {{ $requisition->title }}      |
| Author     | {{ $requisition->author }}     |
@endcomponent
<br>

@component('mail::button', ['url' => route('admin.requests.show', ['request' => $requisition])])
View Request
@endcomponent

<br>
<p>Thanks, have a lovely day.</p>
@endcomponent
