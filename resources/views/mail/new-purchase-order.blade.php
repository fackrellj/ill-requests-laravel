@component('mail::message')
# Hi {{ $user->first_name }}

<p>
A new Purchase Request has been made and approved by the subject librarian for the following item.
</p>

<br>
@component('mail::table')
|        |        |
| :------------- | -------------: |
| Title      | {{ $requisition->title }}      |
| Author     | {{ $requisition->author }}     |
@endcomponent
# Format
@component('mail::table')
| Type       | Edition       | ISBN       | Notes       | Link        |
| :------------- | ------------- | ------------- | ------------- | -------------: |
@foreach($formats as $format)
| {{ $format->type }}      | {{ $format->edition }}      | {{ $format->isbn }}      | {!! $format->notes !!}      | <a href="{{ $format->link }}">View</a>      |
@endforeach
@endcomponent

@component('mail::button', ['url' => route('admin.requests.show', ['request' => $requisition])])
View Request
@endcomponent

<br>
<p>Thanks, have a lovely day.</p>
@endcomponent
