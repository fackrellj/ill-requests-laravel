@component('mail::message')
# Hi {{ $patron->first_name }},

<div>
{!! $body !!}
</div>

<br>
@component('mail::table')
|                |                    |
| :------------- | -------------: |
| <nobr>Title</nobr>        | {{ $requisition->title }}      |
| <nobr>Author</nobr>         | {{ $requisition->author }}     |
@endcomponent
<br>
<p>
Thank you for using the David O. McKay Library!
<br /><br />
<a href="https://library.byui.edu/databases" style="margin-right: 10px;">A - Z List of Databases</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
<a href="https://byui.libcal.com/hours/" style="margin-right: 10px; margin-left: 10px;">Library Hours</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
<a href="http://byui.idm.oclc.org/login?url=http://library.byui.edu/library-chat" style="margin-left: 10px;">Ask A Librarian</a>
</p>
@endcomponent
