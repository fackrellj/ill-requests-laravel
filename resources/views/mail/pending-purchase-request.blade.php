@component('mail::message')
# Hi {{ $user->first_name }}

A new Purchase Request has been made for a(n) {{ $requisition->patron->affiliation }} @if(!is_null($requisition->subject_id))in your area of {{ $requisition->subject->name }}@endif. Please log in to approve this item for purchase or borrowing.

@component('mail::button', ['url' => route('admin.requests.show', ['request' => $requisition])])
View Request
@endcomponent
<br>
@component('mail::table')
|        |        |
| :------------- | -------------: |
| Title      | {{ $requisition->title }}      |
| Author     | {{ $requisition->author }}     |
@endcomponent
<br>
Thanks, have a lovely day!<br>
{{ config('app.name') }}
@endcomponent
