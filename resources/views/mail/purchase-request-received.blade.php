@component('mail::message')
# Hi {{ $patron->first_name }}

<p>
We have just received the following item and it is ready to check out. Please come to the Circulation Desk on the first floor of the David O. McKay Library to check it out.
</p>
<p>
We will release this item after four days if it is not picked up.
</p>
<p>
If you have any questions, please contact Inter-Library Loan at 208-496-9524
</p>

<br>
@component('mail::table')
|        |        |
| :------------- | -------------: |
| Title      | {{ $requisition->title }}      |
| Author     | {{ $requisition->author }}     |
@endcomponent
<br>
<p>
Thank you for using the David O. McKay Library!
<br /><br />
<a href="https://library.byui.edu/databases" style="margin-right: 10px;">A - Z List of Databases</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
<a href="https://byui.libcal.com/hours/" style="margin-right: 10px; margin-left: 10px;">Library Hours</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
<a href="http://byui.idm.oclc.org/login?url=http://library.byui.edu/library-chat" style="margin-left: 10px;">Ask A Librarian</a>
</p>
@endcomponent
