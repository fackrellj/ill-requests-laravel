@component('mail::message')
# Hi {{ $patron->first_name }}

{{--<p>
The following item is ready to pick up at our temporary pick up location just inside the <span style="font-weight: bold;">Southeast entrance of the Manwaring Center</span>. The next pickup time is
@if(now('America/Denver')->dayOfWeek >= 3 && now('America/Denver')->dayOfWeek <= 4)
@if(now('America/Denver')->dayOfWeek == 4)
Today, Thursday, {{ now('America/Denver')->format('M j') }} from 1:00 pm - 3:00 pm
@else
Thursday, {{ (new Carbon\Carbon('next Thursday', 'America/Denver'))->format('M j') }} from 1:00 pm - 3:00 pm
@endif
@else
@if(now('America/Denver')->dayOfWeek == 2)
Today, Tuesday, {{ now('America/Denver')->format('M j') }} from 1:00 pm - 3:00 pm
@else
Tuesday, {{ (new Carbon\Carbon('next Tuesday', 'America/Denver'))->format('M j') }} from 1:00 pm - 3:00 pm
@endif
@endif
</p>
<p>
<img src="https://library.byui.edu/img/maps/mc-2nd-floor.png" alt="Temporary Pick Up Location in Manwaring Center" style="width: 480px; height: 318px;" />
</p>
<p>
If you are unable to come at this time, you will need to re-request the item for a future pickup time.
</p>--}}
{{--
<p>
The following item is ready to pick up at the Main Circulation Desk.
</p>
<p>
All Books are held until 8:00 a.m. the next weekday after receiving this confirmation.
</p>
--}}

<p>
The following item is ready to pick up at the 1st Floor Circulation Desk.
</p>
<p>
All Books are held until 8:00 a.m. the next weekday after receiving this confirmation.
</p>

<br>
@component('mail::table')
|        |        |
| :------------- | -------------: |
| Title      | {{ $pull->title }}      |
| Author     | {{ $pull->author }}     |
@endcomponent
<br>
<p>
Thank you for using the David O. McKay Library!
<br /><br />
<a href="https://library.byui.edu/databases" style="margin-right: 10px;">A - Z List of Databases</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
<a href="https://byui.libcal.com/hours/" style="margin-right: 10px; margin-left: 10px;">Library Hours</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
<a href="http://byui.idm.oclc.org/login?url=http://library.byui.edu/library-chat" style="margin-left: 10px;">Ask A Librarian</a>
</p>
@endcomponent
