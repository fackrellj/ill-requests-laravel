@component('mail::message')
# Hi {{ $patron->first_name }}

<p>
We are pleased to inform you that we have decided to purchase the following item.
</p>
<p>
We will contact you as soon as the item arrives in the library and has been added to the catalog.
</p>
<p>
If you have any questions, please contact Inter-Library Loan at <a href="mailto:ill@byui.edu?subject=Question regarding {{ $requisition->title }}" class="">ill@byui.edu</a>
</p>

<br>
@component('mail::table')
|        |        |
| :------------- | -------------: |
| Title      | {{ $requisition->title }}      |
| Author     | {{ $requisition->author }}     |
@endcomponent
<br>
<p>
Thank you for using the David O. McKay Library!
<br /><br />
<a href="https://library.byui.edu/databases" style="margin-right: 10px;">A - Z List of Databases</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
<a href="https://byui.libcal.com/hours/" style="margin-right: 10px; margin-left: 10px;">Library Hours</a>
&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
<a href="http://byui.idm.oclc.org/login?url=http://library.byui.edu/library-chat" style="margin-left: 10px;">Ask A Librarian</a>
</p>
@endcomponent
