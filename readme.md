## About the McKay Library ILL Manager

This application was designed and built for managing subject librarian purchase approvals from ILL requests and purchase requests.

It also handles faculty and student pull requests.
