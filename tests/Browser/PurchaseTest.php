<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class PurchaseTest extends DuskTestCase
{
    /**
     * Make sure the purchase request form loads.
     *
     * @return void
     */
    public function testPurchaseRequestFormLoading()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('https://maclab.byui.edu/library/purchase-request')
                    ->assertSee('BYU-Idaho')
                    ->type('username', env('BYUI_USERNAME'))
                    ->type('password', env('BYUI_PASSWORD'))
                    ->click('input.login-btn')
                    ->waitForText('Contact Information')
                    ->assertInputValue('name', 'Jon Fackrell')
            ;
        });
    }
}
