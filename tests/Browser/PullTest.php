<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class PullTest extends DuskTestCase
{
    /**
     * Make sure the pull request forms loads.
     *
     * @return void
     */
    public function testPullRequestFormLoading()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://maclab.byui.edu/library/pull-request?title=%0D%0AExploring+Harry+Potter%0D%0A&author=+Schafer%2C+Elizabeth+D.%2C+1965-&location=&call_number=PR830.Y68+S38+2000&platform=ent')
                    ->assertSee('BYU-Idaho')
                    ->type('username', env('BYUI_USERNAME'))
                    ->type('password', env('BYUI_PASSWORD'))
                    ->click('input.login-btn')
                    ->waitForText('Contact Information')
                    ->assertInputValue('name', 'Jon Fackrell')
                    ->assertInputValue('title', 'Exploring Harry Potter')
            ;
        });
    }
}
