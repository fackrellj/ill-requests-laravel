<?php

namespace Tests\Feature\Http\Controllers;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RequisitionControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     *
     * @test
     */
    public function user_must_login()
    {
        $response = $this->get('/home');

        $response->assertRedirect(route('login'));
    }

    /**
     *
     * @test
     */
    public function user_can_view_dashboard()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/home');

        $response->assertStatus(200);
        $response->assertViewIs('admin.ill.requisitions.index');

    }

    /**
     *
     * @test
     */
    public function new_requisition_form_can_be_viewed()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.requests.create'));

        $response->assertStatus(200);
        $response->assertViewIs('admin.ill.requisitions.create');

    }

    /**
     *
     * @test
     */
    public function new_requisition_form_can_be_viewed_with_tn_number()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get(route('admin.requests.create', [
            'tn_number' => '686697',
        ]));

        $response->assertStatus(200);
        $response->assertViewIs('admin.ill.requisitions.create');
        $response->assertViewHas('requisition.tn_number', '686697');

    }
}
