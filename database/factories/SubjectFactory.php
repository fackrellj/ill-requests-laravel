<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Subject;
use Faker\Generator as Faker;

$factory->define(Subject::class, function (Faker $faker) {
    return [
        'purchase_code_id' => $faker->randomNumber(1),
        'name' => $faker->state,
    ];
});
