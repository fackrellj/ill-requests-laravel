<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \App\Enums\Channel;
use App\Enums\Status;
use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Pull::class, function (Faker $faker) {
    $patrons = \App\Patron::all();
    return [
        'patron_id' => $patrons->random()->id,
        'title' => $faker->name,
        'author' => $faker->company,
        'call_number' => $faker->numerify('###########'),
        'barcode' => $faker->numerify('#############'),
        'location' => $faker->randomElement(['1st Floor', '2nd Floor', '3rd Floor']),
        'platform' => $faker->randomElement(['EDS', 'HIP', 'ENTERPRISE']),
    ];
});
