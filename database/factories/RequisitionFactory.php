<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \App\Enums\Channel;
use App\Enums\Status;
use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Requisition::class, function (Faker $faker) {
    $librarians = \App\User::all();
    $subjects = \App\Subject::all();
    $patrons = \App\Patron::all();
    return [
        'librarian_id' => $librarians->random()->id,
        'subject_id' => $subjects->random()->id,
        'patron_id' => $patrons->random()->id,
        'status' => $faker->randomElement(Status::keys()),
        'tn_number' => $faker->randomElement([\Illuminate\Support\Str::random(10)]),
        'title' => $faker->name,
        'author' => $faker->company,
        'channel' => $faker->randomElement(Channel::keys()),
    ];
});
