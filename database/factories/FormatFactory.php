<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Format::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(\App\Enums\Format::keys()),
        'edition' => $faker->year,
        'isbn' => $faker->isbn13,
        'cost' => $faker->randomFloat(2, 10.00, 2000.00),
        'notes' => $faker->sentence,
        'link' => $faker->url,
    ];
});
