<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user1 = factory(App\User::class, 1)->create([
            'name' => 'Mat Miles',
            'username' => 'milesm',
            'email' => 'milesm@byui.edu',
        ])->first();

        factory(App\User::class, 10)->create();

        $user1->assignRole('Super');
    }
}
