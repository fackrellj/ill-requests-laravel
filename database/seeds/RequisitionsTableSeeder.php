<?php

use Illuminate\Database\Seeder;

class RequisitionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Requisition::class, 50)->create()->each(function($requisition){
            $requisition->formats()->saveMany(
                factory(\App\Format::class, array_rand([1,2,3]))->make()
            );
        });
    }
}
