<?php

use Illuminate\Database\Seeder;

class PullsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Pull::class, 50)->create();

    }
}
