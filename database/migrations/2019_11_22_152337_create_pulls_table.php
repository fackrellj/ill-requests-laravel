<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePullsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pulls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('patron_id')->nullable();
            $table->string('title')->nullable();
            $table->string('author')->nullable();
            $table->string('call_number')->nullable();
            $table->string('barcode')->nullable();
            $table->string('location')->nullable();
            $table->string('platform')->nullable();
            $table->mediumText('notes')->nullable();
            $table->boolean('pulled')->default(0);
            $table->boolean('banned')->default(0);
            $table->timestamps();

            $table->foreign('patron_id')
                ->references('id')
                ->on('patrons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pulls');
    }
}
