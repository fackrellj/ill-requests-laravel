<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisitions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('librarian_id')->index()->nullable();
            $table->unsignedBigInteger('subject_id')->index()->nullable();
            $table->unsignedBigInteger('patron_id')->nullable();
            $table->unsignedInteger('status')->nullable();
            $table->string('tn_number')->unique()->nullable();
            $table->string('channel')->nullable();
            $table->string('title')->nullable();
            $table->string('author')->nullable();
            $table->string('purpose')->nullable();
            $table->string('action')->nullable();
            $table->boolean('ill_needs_updated')->default(0);
            $table->boolean('librarian_added_note')->default(0);
            $table->string('approved_for')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->dateTime('purchase_request_sent_at')->nullable();
            $table->dateTime('received_at')->nullable();
            $table->boolean('ill_available')->default(1);
            $table->mediumText('ill_note')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();

            $table->foreign('patron_id')
                ->references('id')
                ->on('patrons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisitions');
    }
}
