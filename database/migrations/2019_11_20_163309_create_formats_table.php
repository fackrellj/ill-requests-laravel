<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('requisition_id');
            $table->string('type')->nullable();
            $table->string('edition')->nullable();
            $table->string('isbn')->nullable();
            $table->float('cost')->nullable();
            $table->mediumText('notes')->nullable();
            $table->mediumText('link')->nullable();
            $table->string('approval')->nullable();
            $table->timestamps();

            $table->foreign('requisition_id')
                ->references('id')
                ->on('requisitions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formats');
    }
}
